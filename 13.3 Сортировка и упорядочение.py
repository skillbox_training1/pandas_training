
import pandas as pd
import numpy as np

df = pd.read_csv('2.csv')
df['total'] = df['w1'] + df['w2'] + df['w3']

print(df.groupby('group').agg(['min', 'max', 'sum']))

# Сортировка данных в таблице метод sort_values (упорядочивается по возрастанию)
print(df.sort_values('total')) # inplace = True будут сохранены изменения в датасете

# Сортировка Датафрейма по нескольким столбцам
print(df.sort_values(['total', 'se3']))

# Если хотим изменить на убывание сортировку, то ascending = False - возрастаюший = False
print(df.sort_values(['total', 'se3'], ascending = False))


