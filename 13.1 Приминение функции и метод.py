
import numpy as np
import pandas as pd

df = pd.read_csv("2.csv")
print(df.head())

df['total'] = df['w1'] + df['w2'] + df['w3']
df['total_gr'] = df['total'] * 1000
print(df)
# Метод Apply позволяет работать с несколькими столбцами.
x = df['total_gr'].apply(np.log)
print(x)

# Метод лок - делает срез необходимых данных из таблицы (1-ый срез - строки, 2-ой срез - столбцы)
# Метод apply вызывает функцию для расчета средней величины, 2-ое значение работа по строкам
# Если в методе apply axis = 0, то расчет средней величины проводился по столбцам.
df['avloss'] = df.loc[:, "w1":'w3'].apply(np.mean, axis=1)
print(df.head())

f = lambda x: x.max() - x.min()
df['wrange'] = df.loc[:, 'w1':'w3'].apply(f, axis=1)
print(df.head())



