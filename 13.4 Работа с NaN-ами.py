
import numpy as np
import pandas as pd

df = pd.read_csv('2.csv')
df['total'] = df['w1'] + df['w2'] + df['w3']
print(df.head())
print(df.info())

# isnull выводит вместо значнеий False or True далле сумируем значения
print(df.isnull().sum())

# Вместо NaN заменяем на необходимое значение
print(df.fillna(0))

# Удаление строчек с пропущенными значениями
df = df.dropna()
print(df.info())