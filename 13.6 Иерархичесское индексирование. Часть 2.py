import pandas as pd

df = pd.DataFrame([
    ['2019-03-11', 'en', 3],
    ['2019-03-11', 'fr', 5],
    ['2019-03-12', 'en', 6],
    ['2019-03-13', 'fr', 1],
    ['2019-03-13', 'en', 2],
    ['2019-03-16', 'fr', 4],
    ['2019-03-17', 'en', 3]], columns = ['date', 'lang', 'n'])

print(df)

df.set_index(['date', 'lang'], inplace=True)
print(df)

print(df.index)

# Сортировка данных по индексу которые были сгруппированы по методу set_index
print(df.sort_index())

# Выбор строки через индентификатор
print(df.loc['2019-03-13'])

D = {'a':1, 'b':2, 'c':3}
for key in D.keys():
    print(key, D[key])

I = iter(D)
print(next(I))
print(next(I))
print(next(I))

R = range(5)
print(R)
I = iter(R)
print(next(I))
print(next(I))
print(list(enumerate("spam")))

L = [1,2,3,4,5]
for i in range(len(L)):
    L[i] += 10
print(L)
















